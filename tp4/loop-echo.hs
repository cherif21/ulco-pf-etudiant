
loopEcho :: Int -> IO String 
loopEcho 0 = return "loop terminated"
loopEcho n = do 
    line <-getLine
    if line == "" then 
        return "empty line"
    else do 
        putStrLn line
        loopEcho (n-1)
        
main :: IO ()
main = do 
    status <- loopEcho 3
    putStrLn status
