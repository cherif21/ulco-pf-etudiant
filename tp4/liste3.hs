
mylast :: [a] -> a
mylast [] = error "liste Vide"
mylast [x] = x
mylast (_:xs) =  mylast xs

myinit :: [a] -> [a]
myinit [] = error "liste vide"
myinit [x] = []
myinit (x:xs) = x : myinit xs

myreplicate :: Int -> [a] -> [a]
myreplicate 1 chaine = chaine
myreplicate ent chaine = chaine ++ myreplicate (ent - 1) chaine

mydrop :: Int -> [a] -> [a]
mydrop ent [] = error "liste vide"
mydrop 0 chaine = chaine 
mydrop ent (x:xs) = mydrop (ent - 1) xs

mytake :: Int -> [a] -> [a]
mytake ent [] = error "liste vide"
mytake 0 chaine = []
mytake ent (x:xs) = [x] ++ mytake(ent - 1) xs

main :: IO()
main = do 
    print(mylast "foobar")
    print(myinit "foobar")
    print(myreplicate 4 "f")
    print (mydrop 3 "foobar")
    print (mytake 3 "foobar")

