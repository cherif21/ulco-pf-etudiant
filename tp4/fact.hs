
fact :: Int -> Int
fact 0 = 1
fact 1 = 1
fact n =  n * fact (n-1)

main :: IO()
main = do 
    print (fact 0)
    print (fact 1)
    print (fact 2)
    print (fact 3)
    print (fact 4)