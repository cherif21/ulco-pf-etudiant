import Data.Char

mylength :: [a] -> Int 
mylength [] = 0 
mylength (_:xs) = 1 + mylength xs

mylength' :: [a] -> Int 
mylength' l = go l 0 
    where 
        go [] acc = acc
        go (_:xs) acc = go xs (acc+1)

toUpperString :: String -> String
toUpperString [] = []
toUpperString (x:xs) = toUpper x : toUpperString xs

onlyLetters :: String -> String
onlyLetters [] = []
onlyLetters (x:xs) = if isLetter x 
    then  x :onlyLetters xs
    else onlyLetters xs 

main :: IO()
main = do
    print (mylength "")
    print (mylength "1234")

    print(mylength' "")
    print (mylength' "1234")

    putStrLn (toUpperString "foo Bar")

    putStrLn (onlyLetters "foo Bar")