

--factTco 0 acc = acc
--factTco n acc = factTco (n-1) (acc*n)
factTco :: Int -> Int 
factTco x = go x 1
    where 
        go 0 acc = acc 
        go n acc = go (n-1) (acc*n)


main :: IO()
main = do 
  

    print (factTco 0)
    print (factTco 1)
    print (factTco 2)
    print (factTco 3)
    print (factTco 4) 