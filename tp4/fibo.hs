import System.Environment
fiboNaive :: Int -> Int 
fiboNaive 0 = 0
fiboNaive 1 = 1
fiboNaive n = fiboNaive (n-1) + fiboNaive (n-2)

fiboTco :: Int -> Int
fiboTco n = go n 1 0
    where 
        go 0 _ a2 = a2
        go x a1 a2 = go (x-1) (a1+a2)  a1

main :: IO ()
main = do 
    args <- getArgs
    case args of 
        ["naive", nstr] -> print (fiboNaive (read nstr))
        ["tco",nstr] -> print (fiboTco (read nstr))
        _ -> putStrLn "usage: <naive|tco> <n>"
