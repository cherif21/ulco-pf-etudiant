doubler :: [Int] -> [Int] 
doubler xs = [x*2 | x <- xs]

pairs :: [Int] -> [Int] 
pairs xs = [x  | x <- xs, even x]

mymap :: (a -> b) -> [a] -> [b]
mymap f xs = [f x | x <- xs]

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter p xs = [x | x <- xs, p x]

multiples :: Int -> [Int] 
multiples n = [x | x <-[1..n], n `mod` x ==0]

combinaisons :: [a] -> [b] -> [(a,b)]
combinaisons xs ys = [(x,y) | x<-xs, y<-ys]

tripletsPyth :: Int -> [(Int, Int, Int)]
tripletsPyth n = [(x,y,z) | x<-[1..n],y<-[1..n],z<-[1..n], x*x+y*y==z*z]

main :: IO() 
main = do 
    print(doubler [1..4])
    print(pairs [1..4])
    print(mymap (*2) [1..4])
    print(myfilter even [1..4])
    print(multiples 35)
    print(combinaisons ["pomme", "poire"][13, 37, 42])
    print(tripletsPyth 13)