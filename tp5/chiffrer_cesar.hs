import Data.Char
decaler :: Int -> Char -> Char
decaler n c =  
    if isLower c
        then  chr(((ord c - ord 'a' + n)`mod` 26)+ ord 'a')
        else c 

chiffrerCesar :: Int -> String -> String 
chiffrerCesar n str = map (\c -> decaler n c) str

main :: IO ()
main = do 
    print (decaler 1 'h')
    print (chiffrerCesar 1 "hal toto")



