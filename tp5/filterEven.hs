import Data.Char
filterEven1 :: [Int] -> [Int] 
filterEven1 [] = []
filterEven1 (x:xs) = if even x 
    then x : filterEven1 xs 
    else filterEven1 xs

filterEven2 :: [Int] -> [Int]
filterEven2 [] = []
filterEven2 xs = filter even xs

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter _ [] = []
myfilter f (x:xs) = if f x 
    then x: myfilter f xs
    else myfilter f xs 

main :: IO ()
main = do
    print(filterEven1 [1..4])
    print(filterEven2 [1..4])
    print(myfilter even [1..4])
    print(myfilter isLetter "[1..4]toto5")