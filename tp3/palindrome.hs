
palindrome :: String -> Bool
palindrome txt = txt==reverse txt

main:: IO ()
main= do 
    print (palindrome "radar")
    print (palindrome "foobar")