import System.Environment

wc :: String -> (Int, Int, Int, Int)
wc input= (nl, nw, nc, ns)
    where 
        nl = length (lines input) 
        nw = length (words input)
        nc = length input
        ns = length (concat (words input))

main :: IO()
main = do
    args <- getArgs
    case args of 
        [filename] -> do 
            content <- readFile filename
            let (nl, nw, nc, ns) = wc content
            putStrLn ("lines: " ++ show nl)
            putStrLn ("words: " ++ show nw)
            putStrLn ("characteres: " ++ show nc)
            putStrLn (" characters (no ws): " ++ show ns)

        _ -> putStrLn usage

usage :: String 
usage = "usage: <file>"