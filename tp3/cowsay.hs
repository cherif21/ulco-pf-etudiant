import System.Environment

cow :: String 
cow = 

    "    \\   ^__^ \n\
    \     \\  (oo)\\_______ \n\
    \        (__)\\       )\\/\\ \n\
    \            ||----w | \n\
    \            ||     || "         

cowsay :: String -> String  
cowsay msg =
  "  "++ replicate len '_' ++"\n"
  ++ "< " ++msg ++ " >\n"
  ++ "  " ++ replicate len '-'++ "\n"
  ++cow
  where len = length msg

main :: IO()
main = do 
    args <- getArgs
    putStrLn (cowsay (unwords args))