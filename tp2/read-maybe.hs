import Text.Read

main :: IO()
main = do
    line <- getLine
    let  n = readMaybe line
    print (n::Maybe Int)
