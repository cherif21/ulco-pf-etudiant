
borneEtFormate1 :: Double -> String
borneEtFormate1 n1= show  n1 ++ " -> " ++ show n2
    where
        n2= if n1 < 0 
            then 0 
            else if n1> 1 
                then 1
                else n1

borneEtFormate2 :: Double -> String
borneEtFormate2 n1 = show  n1 ++ " -> " ++ show n2
    where n2    | n1 < 0 = 0
                | n1 > 1 = 0
                | otherwise = n1
              

main :: IO()
main = do 
    putStrLn (borneEtFormate1 (-1.2))
    putStrLn (borneEtFormate1 0.2)

    putStrLn (borneEtFormate2 (-1.2))
    putStrLn (borneEtFormate2 0.2)

        