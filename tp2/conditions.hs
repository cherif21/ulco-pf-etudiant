
formaterParite :: Int -> String
formaterParite n= 
    if n `mod` 2 == 0 then "pair" else "impair"

formaterSigne :: Int -> String
formaterSigne n= 
    if n > 0 
        then "positif" 
        else if n < 0 
            then "negatif" 
            else "null"

main :: IO()
main=do

    print (formaterParite 21)
    print (formaterParite 42)
    print (formaterSigne 5)
    print (formaterSigne (-4))
    print (formaterSigne 0)

   