
fibo :: Int -> Int
fibo n = case n of 
    0 -> 0
    1 -> 1
    x -> fibo (x-1) + fibo (x-2)

main :: IO()
main = do
    print (fibo 0)
    print (fibo 1)
    print (fibo 10)