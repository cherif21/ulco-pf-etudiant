import Text.Read

safeSqrt :: Double -> Maybe Double
safeSqrt x = 
    if x < 0 
        then Nothing
        else Just (sqrt x)


formatedSqrt :: Double -> String 
formatedSqrt x = "sqrt("++ show x ++ ") "++ result
    where result = case safeSqrt x of
            Nothing -> "is not defined"
            Just r -> "= "++ show r
         
main :: IO()
main = do
    putStrLn (formatedSqrt (-16))
    putStrLn (formatedSqrt 16)
