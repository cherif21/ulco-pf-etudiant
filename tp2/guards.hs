
formaterParite2 :: Int -> String
formaterParite2 n
    | even n = "pair"
    | otherwise = "impair"

formaterSigne2 :: Int -> String
formaterSigne2 n
    | n == 0 ="null"
    | n > 0 = "positif"
    | otherwise = "negatif"

main :: IO()
main=do
    print (formaterParite2 21)
    print (formaterParite2 42)
    print (formaterSigne2 5)
    print (formaterSigne2 (-42))
    print (formaterSigne2 0)