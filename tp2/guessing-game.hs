import System.Random

gess :: Int -> Int -> IO ()
gess secret 0 = putStrLn ("You Lose! Was : 0")
gess secret tries = do 
    putStr ("Type a number : ")
    nstr<-getLine
    let n=read nstr
    putStrLn (VerifSecret secret)
    gess secret (tries-1)

VerifSecret :: Int -> String
VerifSecret secret
    | n == secret ="You Win" 
    | n > secret = "Too big!"
    | otherwise = "Too Small"
    where n = read nstr <-getLine

main :: IO()
main=do
    s <- randomRIO(0,100)
    gess s 5
