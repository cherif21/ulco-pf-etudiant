
formateNul1 :: Int -> String
formateNul1 n = show n ++ " est " ++ fmt n
    where 
        fmt x = case x of 
            0 -> "nul"
            _ -> "non nul"


formateNul2 :: Int -> String
formateNul2 n = show n ++" est " ++ fmt n
    where 
        fmt 0 = "nul"
        fmt _ = "non nul"

main :: IO()
main = do
    putStrLn (formateNul1 0)
    putStrLn (formateNul1 1)

    putStrLn (formateNul2 0)
    putStrLn (formateNul2 1)
