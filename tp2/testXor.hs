
xor1 ::Bool -> Bool -> Bool
xor1  a b = a && (not b) || b && (not a)

xor2 :: Bool -> Bool -> Bool
xor2 a b=
    if a
        then if b then False else True 
        else if b then True else False

xor3 :: Bool -> Bool -> Bool 
xor3 a b= case (a,b) of 
    (True, False)-> True
    (False, True) -> True 
    _ -> False 

xor4 :: Bool -> Bool -> Bool 
xor4 a b
    | a && not b = True 
    | b && not a = True
    | otherwise =False

xor5 :: Bool -> Bool -> Bool 
xor5 True False = True
xor5 False True = True
xor5 _ _ =False

main :: IO ()
main = do
    print (testXor xor1)
    print (testXor xor2)
    print (testXor xor3)
    print (testXor xor4)
    print (testXor xor5)

testXor :: (Bool -> Bool -> Bool) -> Bool 
testXor f = 
    f True True == False &&
    f False False == False && 
    f True False == True &&
    f False True == True
   