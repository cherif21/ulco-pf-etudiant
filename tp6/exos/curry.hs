
myRangeTuple1 :: (Int, Int) -> [Int]
myRangeTuple1 (x0, x1) = [x0..x1]

myRangeTuple2 :: Int -> [Int]
myRangeTuple2 x1 = myRangeTuple1 (0, x1)

myRangeCurry1 :: Int -> Int ->  [Int]
myRangeCurry1 x0 x1 = [x0..x1]

myRangeCurry2 :: Int -> [Int]
myRangeCurry2 x1 = myRangeCurry1 0 x1

main :: IO ()
main = do 
    print(myRangeTuple1 (0,9))
    print(myRangeCurry1 0 9)
    print(myRangeTuple2 9)
    print(myRangeCurry2 9)