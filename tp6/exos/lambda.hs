

main :: IO()
main = do 
    print(map (\x->2*x)[1..4])
    print(map(\x ->x/2)[1..4])
    print(map(\(c,n)->c : '-' : show n)(zip['a'..'z'][1..]))
    print(zipWith (\c n->c : '-' : show n)['a'..'z'][1..])
