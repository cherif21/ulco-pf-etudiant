import Data.List

type Vector = [Char]
type Matrix =[[Char]]
operations :: [(Matrix->Vector, Matrix ->Matrix)]
operations =cycle
    [ (head,transpose.tail),
    (last,transpose.init),
    (reverse.last,transpose.init),
    (reverse.head,transpose.tail)
    ]

spiral :: Matrix -> Vector 
spiral m0 = go m0 operations
    where 
        go [] _= []
        go mi ((o1,o2):ops) = o1 mi ++ go (o2 mi) ops
        go _ [] =error ""



m :: Matrix
m =[    ['a','b','c','d'],
        ['e','f','g','h'],
        ['i','j','k','l'],
        ['m','n','o','p']
    ]


main :: IO ()
main = do 
    print m
    print (spiral m)
   
   

