import Text.Read
import System.Environment
import System.IO

type Task = (Int, Bool, String) --numero, realiser ou pas , description
type Todo = (Int, [Task]) --numero de la prochaine tache a ajouter

formatTask:: Task -> String 
formatTask (num,etat,desc) = do
    if etat then
        "[X] "++show num ++ ". "++desc
    else 
        "[-] "++show num ++". "++desc

printAllTask:: Todo -> IO()
printAllTask todo = do
    mapM_ (putStrLn . formatTask) (snd todo)

exit:: IO()
exit =  putStrLn ("Fin du programme")

usage :: IO()
usage =
    putStrLn "usage: <filename>"

menu :: IO ()
menu = do
    putStrLn "usage:"
    putStrLn "print"
    putStrLn "print todo"
    putStrLn "print done"
    putStrLn "add <string>"
    putStrLn "undo <int>"
    putStrLn "del <int>"
    putStrLn "exit"


printDone :: Task -> String
printDone (num,etat,desc) = 
     if etat then
        "[X] "++show num ++ ". "++desc
    else ""

printTodo:: Task -> String
printTodo (num,etat,desc) = 
     if etat then
        "[-] "++show num ++ ". "++desc
    else ""

commandeList :: Todo-> IO()
commandeList todo= do
    name <-getLine
    case name  of 
        "print" -> printAllTask todo
        "print Done" -> mapM_ (putStrLn.printDone)(snd todo)
        "print Todo" ->mapM_ (putStrLn.printTodo)(snd todo)
        "exit" -> exit
        _ -> menu

 
main :: IO ()
main = do
    args <- getArgs
    case args of
        [filename] -> do
            contents <- readFile filename
            menu 
            commandeList (read contents)
        _ -> usage
    


   
   

