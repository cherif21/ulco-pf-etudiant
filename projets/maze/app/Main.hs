import qualified Data.Vector as V

import Control.Monad (forM_)
import System.Environment
import System.IO

type Maze = V.Vector Char

-- runMaze :: Int -> Int -> Int -> Int -> Maze -> IO ()
-- runMaze ni nj i0 j0 maze = ...

mazeDisplay :: Int -> Int -> Int ->Int ->  Maze -> IO()
mazeDisplay h c posh posc maze = do
    forM_ [0..h-1] $ \i -> do
        forM_ [0..c-1] $ \j -> do
            let pos = i*c+j
            let car = maze V.!pos
            if (pos ==posh*c+posc && [car] == " ")
                    then 
                        putStr "0"
            else do
                if [car]=="0" then putStr "0"
                else putStr [car]
        putStr "\n"

exitMaze :: Int -> Int->Int-> Maze -> Bool
exitMaze c posh posc maze = do 
    let pos=posh*c+posc
    let car = maze V.!pos
    if [car]=="?" then  True
    else False

looserMaze :: Int -> Int->Int-> Maze -> Bool
looserMaze c posh posc maze = do 
    let pos=posh*c+posc
    let car = maze V.!pos
    if [car]=="+" then  True
    else False

playMaze :: Int -> Int -> Int -> Int -> Maze -> IO()
playMaze h c hor cor maze = do
    putStrLn "\npress a key..."
    x <- getChar
    putStrLn ("\nyou pressed: " ++ [x])

    case x of 
        'g' ->do
            mazeDisplay h c hor (cor-1) maze 
            if exitMaze c hor (cor-1) maze then putStrLn "You Win!!!!!"
            else if looserMaze c hor (cor-1) maze then putStrLn "You are a loser!!!!!"
            else playMaze h c hor (cor-1) maze 
        'd' ->do
            mazeDisplay h c hor (cor+1) maze
            if exitMaze c hor (cor+1) maze then putStrLn "You Win!!!!!"
            else if looserMaze c hor (cor+1) maze then putStrLn "You are a loser!!!!!"
            else playMaze h c hor (cor+1) maze 
        'h' -> do
            mazeDisplay h c (hor-1) cor maze
            if exitMaze c (hor-1) cor maze then putStrLn "You Win!!!!!"
            else if looserMaze c (hor-1) cor maze then putStrLn "You are a loser!!!!!"
            else playMaze h c (hor-1) cor maze 
        'b' ->do 
                mazeDisplay h c (hor+1) cor maze
                if exitMaze c (hor+1) cor maze then putStrLn "You Win!!!!!"
                else if looserMaze c (hor+1) cor maze then putStrLn "You are a loser!!!!!"
                else playMaze h c (hor+1) cor maze 
        _ ->do
            mazeDisplay h c hor cor maze
            playMaze h c hor cor maze

   

main :: IO ()
main = do
    hSetBuffering stdin NoBuffering
    args <-getArgs
    case args of
        [filename] -> do 
            file <- readFile filename
            let (infoLine:mazeLines) = lines file
                maze = V.fromList (concat mazeLines)
            print infoLine
            let ligne = words infoLine
            let h=read(ligne !! 0)::Int
            let c=read(ligne !! 1)::Int
            let posh=read(ligne !! 2)::Int
            let posc=read(ligne !! 3)::Int
            mazeDisplay h c posh posc maze
            playMaze h c posh posc maze
        _-> putStrLn "usage: <filename>"
    
    
    
    
  

