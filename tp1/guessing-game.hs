import System.Random

gess :: Int -> Int -> IO ()
gess secret 0 = putStrLn ("You Lose! Was : "++ show secret)
gess secret tries = do
    putStr ("Type a number : ")
    nstr<-getLine
    let n=read nstr
    if n == secret then putStrLn ("You Win")
        else do 
            putStrLn( if n>secret then "Too big!" else "Too Small!")
            gess secret (tries-1)

main :: IO()
main=do
    s <- randomRIO(0,100)
    gess s 10