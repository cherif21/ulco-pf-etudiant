add2Int :: Int->Int->Int
add2Int n1 n2=n1+n2

add2Double :: Double->Double->Double
add2Double n1 n2=n1+n2

add2Num :: Num a=>a->a->a
add2Num n1 n2=n1+n2

u :: Int
u=2

v :: Double
v=2

main::IO()
main=do
    print (add2Int u u)
    print (add2Double v v)
    print (add2Num u u)
    print (add2Num v v)