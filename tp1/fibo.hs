import System.Environment

fibo :: Int -> Int
fibo 0=0
fibo 1=1
fibo n=fibo(n-1) + fibo (n-2)

main :: IO()
main=do
    args <-getArgs
    let nbargs = length args
    let n =read  (head args)::Int
    if nbargs==1
        then do
            let msg="f("++ show (n) ++") = "++ show (fibo n) ++"."
            print msg
        else do 
            print("usage: <n>")

    

